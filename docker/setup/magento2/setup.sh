#!/bin/bash
WWW_DIR=/var/www/html
cd /var/www/html;
if ! [[ "$1" = "import" ]]; then
    if ! [[ "$1" = "install" ]]; then
        echo -e "${RED}Es wurde der Parameter 'install' oder 'import' erwartet!${NORMAL}";
        exit 1;
    fi
fi

if [[ "$1" = "import" ]]; then
    if [ -f /var/www/sql/magento.sql ]; then
      echo "${RED}Magento kann nicht importiert werden, wenn die Magento Datenbank nicht vorhanden ist. (shared/sql/magento.sql existiert nicht)."
      echo "Magento kann ohne Datenbank nur komplett installiert werden!${NORMAL}"
      exit 1;
    fi
fi

if [ "$(ls -A $WWW_DIR)" ]; then
     echo "Das Magento-Verzeichnis ist nicht leer. Sollen alle Dateien unwiderruflich gelöscht werden?"
     read -n 1 -p "Löschen? (y/n): " AMSURE
     if [ "$AMSURE" = "y" ]; then
        cd ..;
        rm html -r;
        mkdir html;
        cd html;
     else
        echo "Installation abgebrochen!";
        exit 1;
     fi
fi
set -e

echo "Magento wird nun herunterladen ..."
composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition=$M2SETUP_VERSION .
echo "Magento wurde heruntergeladen!"

chmod +x ./bin/magento

if [[ "$1" = "install" ]]; then
    echo "Magento wird nun installiert!";
    php -d memory_limit=2G ./bin/magento setup:install \
      --db-host=$DB_HOST \
      --db-name=$DB_NAME \
      --db-user=$DB_USER \
      --db-password=$DB_PASSWORD \
      --base-url=$M2SETUP_BASE_URL \
      --admin-firstname=$M2SETUP_ADMIN_FIRSTNAME \
      --admin-lastname=$M2SETUP_ADMIN_LASTNAME \
      --admin-email=$M2SETUP_ADMIN_EMAIL \
      --admin-user=$M2SETUP_ADMIN_USER \
      --admin-password=$M2SETUP_ADMIN_PASSWORD
    echo "Magento wurde installiert!";
else
    echo "Import Magento Datenbank - Bitte nicht wundern, ich muss mir noch eben einen MySQL-Client installieren. Den deinstalliere ich danach wieder!";
    sleep 3;
    apt-get update;
    apt-get install -y mysql-client;
    mysql -h$DB_HOST -u$DB_USER -p$DB_PASSWORD $DB_NAME < /var/www/sql/magento2-import-ready.sql
    echo "Datenbankimport erfolgreich!";
    echo "Deinstallation von MySQL Client!";
    sleep 1;
    apt-get purge mysql-client;
    apt-get autoremove;
    echo "Der MySQL Client ist nun wieder deinstalliert!";
fi



mv $WWW_DIR/bin/magento $WWW_DIR/bin/magento-php;
cp /home/setup_assets/magento /usr/local/bin/magento; #//TODO wurde nicht kopiert
chmod a+x /usr/local/bin/magento;

echo "Der Cronjob wird nun eingerichtet";
echo "*/1 * * * * su -c \"php $WWW_DIR/update/cron.php\" -s /bin/sh www-data > /proc/1/fd/2 2>&1" | crontab - \
  && (crontab -l ; echo "*/1 * * * * su -c \"php $WWW_DIR/bin/magento-php cron:run\" -s /bin/sh www-data > /proc/1/fd/2 2>&1") | crontab - \
  && (crontab -l ; echo "*/1 * * * * su -c \"php $WWW_DIR/bin/magento-php setup:cron:run\" -s /bin/sh www-data > /proc/1/fd/2 2>&1") | crontab -
echo "Der Cronjob wurde eingerichtet";

echo "Setze Vereichnisberechtigungen";
chmod 0777 /var/www/* -R;

echo "Deploye Magentos Static-Content";
magento setup:static-content:deploy;#//TODO wurde durchgeführt

echo "Das Setup ist erfolgreich gewesen"


