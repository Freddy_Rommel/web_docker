#!/bin/bash
NORMAL='\033[0m'     #  ${NORMAL}
RED='\033[0;31m'     #  ${RED}
GREEN='\033[0;32m'   #  ${GREEN}
YELLOW='\033[0;33m'  #  ${YELLOW}
BLUE='\033[0;34m'    #  ${BLUE}
MAGENTA='\033[0;35m' #  ${MAGENTA}
CYAN='\033[0;36m'    #  ${CYAN}
GRAY='\033[0;37m'    #  ${GRAY}

PROJECT_NAME=interbranchedocker

function StartDocker() {
    ./docker-compose --project-name ${PROJECT_NAME} up -d
    #docker exec "$1_phpfpm_1" /srv/__scripts/run.sh
}
function StopDocker() {
    ./docker-compose --project-name ${PROJECT_NAME} stop
}

function RestartDocker() {
    StopDocker $1
    StartDocker $1
}
function DeployDocker() {
    if [[ "$2" = "DE" ]]; then
        docker exec "$1_webserver_1" magento setup:static-content:deploy de_DE
    fi
    if [[ "$2" = "EN" ]]; then
        docker exec "$1_webserver_1" magento setup:static-content:deploy en_US
    fi
    if [[ "$2" = "" ]]; then
        docker exec "$1_webserver_1" magento setup:static-content:deploy en_US
        docker exec "$1_webserver_1" magento setup:static-content:deploy de_DE
    fi

    #docker stop "$1_nodegrunt_1"
    #docker start "$1_nodegrunt_1"
}
function EnableModules() {
    docker exec "$1_phpfpm_1" magento deploy:mode:set developer
    docker exec "$1_phpfpm_1" magento module:enable --all
    docker exec "$1_phpfpm_1" magento setup:upgrade
    docker exec "$1_phpfpm_1" magento cache:clean
    DeployDocker $1
}
function ImportDb() {
    echo -en "${RED}Import DB?${NORMAL}"
    read -n 1 -p "(y/n): " AMSURE
    if [[ "$AMSURE" = "y" ]]; then
        docker exec -ti "$1_db_1" bash /srv/scripts/import-db.sh "$2"
        docker exec "$1_phpfpm_1" magento cache:clean
    fi
}

function showthemes() {
    echo -e "${GREEN}Show Themes?${NORMAL}"
    read -n 1 -p "(y/n): " AMSURE
    if [[ "$AMSURE" = "y" ]]; then
       docker exec "${PROJECT_NAME}_db_1" mysql -umagento2 -pmagento2 magento2 -e "use magento2; SELECT theme_id, theme_title FROM theme WHERE 1;"
    fi
}

function showother() {
    echo -e "${GREEN}Show theme.js?${NORMAL}"
    read -n 1 -p " (y/n): " AMSURE
    if [[ "$AMSURE" = "y" ]]; then
        docker exec "${PROJECT_NAME}_phpfpm_1" cat /srv/www/dev/tools/grunt/configs/themes.js
    fi

    echo -en "${GREEN}Show less files? ${NORMAL}"
    read -n 1 -p " (y/n): " AMSURE
    if [[ "$AMSURE" = "y" ]]; then
        docker exec "${PROJECT_NAME}_phpfpm_1" ls -la /srv/www/pub/static/frontend/Interbranche/interbranche/de_DE/css/source/extends
    fi

    echo -en "${GREEN}Start Grunt Logs?${NORMAL}"
    read -n 1 -p " (y/n): " AMSURE
    if [[ "$AMSURE" = "y" ]]; then
        docker logs -f "${PROJECT_NAME}_nodegrunt_1"
    fi
}

if [[ $# -lt 1 ]]; then
    echo "Usage:"
    echo -en "${YELLOW}run\t|\nstart\t|\nrestart\t|\nstop\t|${NORMAL}\t - Magento 2\n"
    echo -en "${YELLOW}bash${NORMAL}\t\t - to container ${PROJECT_NAME}_phpfpm_1\n"
    echo -en "${YELLOW}setup${NORMAL}\t\t - Setup new Magento 2\n"
    echo -en "${YELLOW}delete${NORMAL}\t\t - Delete all container with name '${PROJECT_NAME}_*'\n"
    echo -en "${YELLOW}dbimport [dev|live]${NORMAL}\t - Import DB (file > DB)\n"
    echo -en "${YELLOW}dbexport${NORMAL}\t - Export DB (DB > file)\n"
    echo -en "${YELLOW}cleancache${NORMAL}\t - clean cache\n"
    echo -en "${YELLOW}gruntwatch${NORMAL}\t - logs gruntwatch\n"
    echo -en "${YELLOW}enablemodules${NORMAL}\t - Enable all Modules\n"
    echo -en "${YELLOW}deploy [DE|EN]${NORMAL}\t - static-content:deploy\n"
fi

if [[ "$1" = "bash" ]]; then
	docker exec -ti "${PROJECT_NAME}_webserver_1" bash
fi


#    Verwendung:
#    ./docker.sh setup import - Läd Magento herunter, installiert es aber nicht. Stattdessen wird die Datenbank importiert.
#    ./docker.sh setup install - Läd Magento herunter und installiert es. Ein Datenbankimport findet nicht statt.
if [[ "$1" = "setup" ]]; then

    if ! [[ "$2" = "import" ]]; then
        if ! [[ "$2" = "install" ]]; then
            echo -e "${RED}Es wurde der Parameter 'install' oder 'import' erwartet!${NORMAL}";
            exit
        fi
    fi

    echo -en "${GREEN}apache2 - stop${NORMAL}\n"
    sudo service apache2 stop
    StopDocker
    sed -i -e 's|\(.*\)\(#volume_remove_on_magento_setup\)|#\1\2|' docker-compose.yml
    sed -E 's/DEFINER=`[^`]+`@`[^`]+`/DEFINER=CURRENT_USER/g' shared/sql/magento2.sql > shared/sql/magento2-import-ready.sql
    ./docker-compose build --no-cache;
    StartDocker
    docker exec -ti  "${PROJECT_NAME}_webserver_1" /home/setup $2
    StopDocker
    sed -i -e 's|#\(.*\)\(#volume_remove_on_magento_setup\)|\1\2|' docker-compose.yml
    rm shared/sql/magento2-import-ready.sql;
    StartDocker
fi

if [[ "$1" = "cleancache" ]]; then
    docker exec "${PROJECT_NAME}_webserver_1" magento cache:clean
fi

if [[ "$1" = "enablemodules" ]]; then
    EnableModules ${PROJECT_NAME}
fi

if [[ "$1" = "restart" ]]; then
    read -n 1 -p "RESTART? (y/n): " AMSURE
    [ "$AMSURE" = "y" ] || exit
	RestartDocker ${PROJECT_NAME}
fi

if [[ "$1" = "run" || "$1" = "start" ]]; then
    StartDocker ${PROJECT_NAME}
fi

if [[ "$1" = "stop" ]]; then
    read -n 1 -p "STOP? (y/n): " AMSURE
    [ "$AMSURE" = "y" ] || exit
	StopDocker ${PROJECT_NAME}
fi

if [[ "$1" = "deploy" ]]; then
    DeployDocker ${PROJECT_NAME} $2
fi

if [[ "$1" = "delete" ]]; then
    echo -e "${RED}Delete all container with name: '${PROJECT_NAME}_*':${NORMAL} \n"
    read -n 1 -p "are you sure? (y/n): " AMSURE
    [ "$AMSURE" = "y" ] || exit
    StopDocker ${PROJECT_NAME}
    docker rm $(docker ps -a -q -f name=${PROJECT_NAME}_)

    echo -en "${BLUE}we put back ./docker-compose.yml?${NORMAL}"
    read -n 1 -p "(y/n): " AMSURE
    if [[ "$AMSURE" = "y" ]]; then
        cp ./docker-compose.yml.orig ./docker-compose.yml
    fi
fi

if [[ "$1" = "gruntwatch" ]]; then
    docker logs -f "${PROJECT_NAME}_nodegrunt_1"
fi

if [[ "$1" = "dbimport" ]]; then
    ImportDb ${PROJECT_NAME} "$2"
fi

if [[ "$1" = "dbexport" ]]; then
    echo -en "${RED}Export DB > file(magento2.sql)?${NORMAL}"
    read -n 1 -p "(y/n): " AMSURE
    if [[ "$AMSURE" = "y" ]]; then
        docker exec -ti "${PROJECT_NAME}_db_1" bash /srv/scripts/export-db.sh
    fi
fi


if [[ "$1" = "install-docker-engine" ]]; then
    echo -en "${BLUE}Es wird nun die Docker-Engine eingerichtet. Dafür müssen ein paar Sachen vorbereitet. Dies kann bis zu 15 Minuten dauern!${NORMAL}"
    echo -e "${BLUE}Soll mt der einrichtung nun begonnen werden?${NORMAL}"
    read -n 1 -p "(y/n): " AMSURE
    if [[ "$AMSURE" = "y" ]]; then
        //TODO
    fi
fi